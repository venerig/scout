# Scout automation
![robotframework](https://img.shields.io/badge/powered%20by-robotframework-brightgreen) 

This is the repository for Automation related to Scout project.
This project is a bot that update the anagrafica data for a personal account.

## Usage

To run, you can choose to `git clone`. 


## Available test variables

This are the project variables:
- ${email_text}  yourmail@yourprovider.com
- ${text_to_edit}  your_text_to_edit
- ${site_password}  your password

 You can find it inside the .robot file.


## Running a specific file

You can run the bot as single job with the following command: 
```
robot -d /Result -L info -b debug.log -t "site_one" scout.robot
```
Or you can run the scraper as parallel suite (using Pabot) with the following command: 
```
pabot --testlevelsplit --processes 4 --outputdir /Result/Pabot scout.robot 
```

## Test sections

There is only one .robot file inside the project, here the project structure:
```
Scout/
    /scout.robot
    /README
```
<!-- NB: A dummy file is left on the tests/ root to prevent testing all files on commit. -->
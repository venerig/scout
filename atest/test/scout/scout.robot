*** Settings ***
Documentation   Scout
Resource        ../../src/common.resource
Test Setup      Open Chrome Headless
Test Teardown   Close Browser
Test Template   Update site
Force Tags      DEBUG


*** Test Cases ***
Site_A   ${URL_A}   ${site_a.login}  ${site_a.email}
...  ${site_a.pwd}  ${URL_A_PWD}   ${site_a.signin}
...  ${site_a.edit}  ${site_a.edit_1}  ${EMPTY}
...  ${site_a.text}  ${site_a.save}  ${site_a.msg}
Site_B    ${URL_B}   ${site_b.login}  ${site_b.email}
...  ${site_b.pwd}  ${URL_B_PWD}   ${site_b.signin}
...  ${site_b.edit}  ${EMPTY}  ${EMPTY}
...  ${site_b.text}  ${site_b.save}  ${EMPTY}
